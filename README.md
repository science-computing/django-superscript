# Django SuperScript

SuperScript is a small Django app that allows saving and execution of generic python scripts by site superusers within the django instance's environment. It is an extremely sharp knife, and like all sharp knives it is very dangerous, very useful, and very satisfying.

## Quick Start
After installing superscript via pip, add it to your `settings.py` file:
~~~
INSTALLED_APPS = [
    ...
    'superscript',
]
~~~~
You will also need to add the superscript urls to your project's `urls.py` file:
~~~~
url(r'^/admin/superscript/', include('superscript.urls')),
~~~~
Run `python manage.py migrate` to add the Scripts model to your database, and you're done!
Users with the necessary permissions will now be able to view, edit, and create scripts -- which will be runnable within the Django admin by superusers.

## A Warning
As stated before, this tool is extremely dangerous. It allows site superusers to run **any** python scripts they wish to run, up to and including deleting, corrupting, or modifying system files.

**DO NOT INSTALL THIS PACKAGE ON SYSTEMS WHERE THE SITE ADMIN IS NOT ALSO THE SYSTEM ADMIN**
