from django.contrib import admin
from .models import Script
from .forms import ScriptForm


class ScriptAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'date_modified')
    fields = ('submission_url', 'title', 'slug', 'description', 'source')
    readonly_fields = ('slug',)
    model = Script
    form = ScriptForm


admin.site.register(Script, ScriptAdmin)
