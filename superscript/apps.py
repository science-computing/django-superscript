from django.apps import AppConfig


class SuperscriptConfig(AppConfig):
    name = 'superscript'
