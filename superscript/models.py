from django.db import models
from django.utils.text import slugify
from .execute import execute_script


class Script(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=255, unique=True)
    description = models.CharField(max_length=255, blank=True)
    slug = models.SlugField(max_length=255, blank=True)
    source = models.TextField(blank=True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Script, self).save(*args, **kwargs)

    def execute(self, update_status=True):
        output = execute_script(self.source)
        return output
