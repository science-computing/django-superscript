$(function(){

    var editor = CodeMirror.fromTextArea(document.getElementById("id_source"), {
        lineNumbers: true,
        mode: "python",
        extraKeys: {
            "Cmd-Enter": function(instance) {
                submitCode();
                return false;
            },
            "Ctrl-Enter": function(instance) {
                submitCode();
                return false;
            }
        }
        });

    $('#id_title').on('input', function(){
        var val = this.value;
        $('.field-slug').find('p').text(URLify(val))
    })

    // Create the submit button and response thingy
    var rowdiv = $("<div class='form-row'></div>")
    var indiv = $("<div></div>");
    var button = $("<button type='button' class='adminbutton'>Run Script</button>")
    var outputtext = $('<code>Ctrl+Enter to run a script</code>')
    indiv.append($("<label></label>").append(button).bind('click', submitCode))
    indiv.append($("<div class='script-output'></div>").append(outputtext))

    rowdiv.append(indiv)
    $('#id_source').closest('.form-row').after(rowdiv)

    function submitCode(){
        var postdata = {'csrfmiddlewaretoken': $( "input[name='csrfmiddlewaretoken']" ).val(),
                                    'source': editor.getValue()}
        $.post($('#id_submission_url').val(), postdata, function(data){
            outputtext.text(data)
        })
    }



})
