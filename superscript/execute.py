import sys
import traceback
from contextlib import contextmanager
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO


@contextmanager
def catch_stdout(buff):
    stdout = sys.stdout
    sys.stdout = buff
    yield
    sys.stdout = stdout


def execute_script(source):
    buff = StringIO()
    try:
        with catch_stdout(buff):
            exec(source)
    except:
        traceback.print_exc(file=buff)

    return buff.getvalue()
