from django.http import HttpResponse
from django.views.decorators.http import require_POST
from django.contrib.auth.decorators import permission_required
from .execute import execute_script


@require_POST
@permission_required('is_superuser')
def execute_script_view(request):
    source = request.POST.get('source', '')
    output = execute_script(source)
    return HttpResponse(output)
