from django import forms
from django.urls import reverse
from .models import Script


class ScriptForm(forms.ModelForm):
    submission_url = forms.CharField(widget=forms.HiddenInput(), required=False)

    def __init__(self, *args, **kwargs):
        # instance = kwargs.get('instance', None)
        kwargs.update(initial={
            'submission_url': reverse('superscript-blank-submission-url')
        })
        super(ScriptForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Script
        fields = ['submission_url', 'title', 'slug', 'description', 'source']

    class Media:
        css = {'all': ('superscript/css/admin_form.css', 'superscript/css/codemirror.min.css',)}
        js = ('superscript/js/jquery.min.js',
              'superscript/js/codemirror.min.js',
              'superscript/js/python.min.js',
              'superscript/js/admin_form.js')
