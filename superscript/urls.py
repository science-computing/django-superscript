from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^superscript-blank-submission/$',
        views.execute_script_view,
        name='superscript-blank-submission-url'),
]
